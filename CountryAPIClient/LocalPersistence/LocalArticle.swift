//
//  LocalArticle.swift
//  CountryAPIClient
//
//  Created by Main User on 31.07.2021.
//  Copyright © 2021 SwiftFlutter. All rights reserved.
//

import Foundation
import RealmSwift

class LocalArticleData: Object {
    @Persisted var status: String?
    @Persisted var totalResults: Int?
    
    @Persisted var articles: List<LocalArticle>    
}

class LocalArticle: Object {
    @Persisted(primaryKey: true) var _id: ObjectId
    @Persisted var source: LocalSource?
    @Persisted var author: String?
    @Persisted var title: String?
    @Persisted var articleDescription: String?
    @Persisted var url: String?
    @Persisted var urlToImage: String?
    @Persisted var publishedAt: Date?
    @Persisted var content: String?
   // @Persisted var isFavorite: Bool? = false
}

class LocalSource: Object {
   @Persisted var id: String?
   @Persisted var name: String?
}
