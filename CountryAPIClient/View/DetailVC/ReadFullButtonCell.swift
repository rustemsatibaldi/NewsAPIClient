//
//  ReadFullButtonCell.swift
//  CountryAPIClient
//
//  Created by Main User on 29.07.2021.
//  Copyright © 2021 SwiftFlutter. All rights reserved.
//

import UIKit

class ReadFullButtonCell: UITableViewCell {
    
    var urlFromJSON = ""
    
    lazy var goToWebsiteButton:UIButton = {
        let goToWebsiteButton = UIButton()
        goToWebsiteButton.setTitle("Read full article", for: .normal)
        goToWebsiteButton.setTitleColor(.white, for: .normal)
        goToWebsiteButton.backgroundColor = UIColor(named: "greenCarib")
      //  goToWebsiteButton.layer.borderWidth = 1.5
      //  goToWebsiteButton.layer.borderColor = UIColor.black.cgColor
        goToWebsiteButton.layer.cornerRadius = 8
        return goToWebsiteButton
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupConstraints() {
        contentView.addSubview(goToWebsiteButton)
        
        goToWebsiteButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            goToWebsiteButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30),
            goToWebsiteButton.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 70),
            goToWebsiteButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -70),
            //    goToWebsiteButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            goToWebsiteButton.widthAnchor.constraint(equalToConstant: 60),
            goToWebsiteButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
}

