//
//  ArticleDetailTableCell.swift
//  CountryAPIClient
//
//  Created by Main User on 26.11.2020.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit

class ArticleDetailTableCell: UITableViewCell {

 lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        
        return titleLabel
    }()
    
    
    
    
   lazy var contentTextView:UITextView = {
        let contentTextView = UITextView()
        contentTextView.isEditable = false
    contentTextView.backgroundColor = UIColor(named: "Honeydew")
    
    //  contentTextView.isScrollEnabled = false
    contentTextView.font = UIFont(name: "Roboto-Regular", size: 17)
        return contentTextView
    }()
    
    //MARK: - Setup constraints
    func setupConstraints() {
        contentTextView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentTextView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
              contentTextView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                contentTextView.topAnchor.constraint(equalTo: contentView.topAnchor),
                  contentTextView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(contentTextView)
        setupConstraints()

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
