//
//  ArticelDetailHeaderView.swift
//  CountryAPIClient
//
//  Created by Main User on 26.11.2020.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit

class ArticleDetailHeaderView: UIView {
    
    let detailImageView: UIImageView = {
        let detailImageView = UIImageView()
        detailImageView.clipsToBounds = true
        detailImageView.contentMode = .scaleToFill
        return detailImageView
    }()
        
  /*  lazy var saveToFavoritesButton:UIButton = {
        let saveToFavoritesButton = UIButton()
        saveToFavoritesButton.setImage(UIImage(named: "search"), for: .normal)
        saveToFavoritesButton.setImage(UIImage(named: "saved"), for: .selected)
        saveToFavoritesButton.addTarget(self, action: #selector(favoriteBtnTapped), for: .touchUpInside)
        saveToFavoritesButton.tintColor = .green
        return saveToFavoritesButton
    }()*/
        
    override init(frame: CGRect){
          super.init(frame: frame)
        self.backgroundColor = .systemYellow
           self.addSubview(detailImageView)
         //  self.addSubview(saveToFavoritesButton)
           setupContraints()
    }
      required init?(coder aDecoder: NSCoder) {
          fatalError("init(coder:) has not been implemented")
      }
    
    //MARK: - Setup Constraints
    func setupContraints() {
           detailImageView.translatesAutoresizingMaskIntoConstraints = false
       //    saveToFavoritesButton.translatesAutoresizingMaskIntoConstraints = false
        /*  NSLayoutConstraint.activate([
               headerView.leftAnchor.constraint(equalTo: self.leftAnchor),
               headerView.topAnchor.constraint(equalTo: self.topAnchor),
               headerView.rightAnchor.constraint(equalTo: self.rightAnchor),
               headerView.bottomAnchor.constraint(equalTo: self.topAnchor),
               headerView.heightAnchor.constraint(equalToConstant: 350)
           ])*/
           
           NSLayoutConstraint.activate([
               detailImageView.leftAnchor.constraint(equalTo: self.leftAnchor),
               detailImageView.topAnchor.constraint(equalTo: self.topAnchor),
               detailImageView.rightAnchor.constraint(equalTo: self.rightAnchor),
            detailImageView.heightAnchor.constraint(equalToConstant: 320)
           ])
                   
          /* NSLayoutConstraint.activate([
               saveToFavoritesButton.topAnchor.constraint(equalTo: detailImageView.topAnchor, constant: 90),
                      saveToFavoritesButton.rightAnchor.constraint(equalTo: detailImageView.rightAnchor, constant: -30),
                      saveToFavoritesButton.heightAnchor.constraint(equalToConstant: 70)
           ])*/
     }
    
    //MARK: - Selectors
 @objc func favoriteBtnTapped() {
  //  saveToFavoritesButton.isSelected = !saveToFavoritesButton.isSelected
    
    UserDefaults.standard.bool(forKey: "isSaved")
            UserDefaults.standard.synchronize()
    }
}

