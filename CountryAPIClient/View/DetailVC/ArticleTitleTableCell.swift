//
//  ArticleTitleCell.swift
//  CountryAPIClient
//
//  Created by Main User on 26.11.2020.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit

class ArticleTitleTableCell: UITableViewCell {
   
     /*  lazy var titleLabel:UILabel = {
       let titleLabel = UILabel()
       // titleLabel.numberOfLines = 0
       // titleLabel.font.withSize(19)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
      //  titleLabel.lineBreakMode = .byWordWrapping
        return titleLabel
    }()*/
    
    lazy var titleTextView:UITextView = {
        let textView = UITextView()
        textView.font = UIFont(name: "NewsCycle-Bold", size: 20)
        textView.allowsEditingTextAttributes = false
      //  textView.backgroundColor = UIColor(named: "Cultured")
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
     //   contentView.addSubview(titleLabel)
        contentView.addSubview(titleTextView)
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupConstraints() {
       // titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleTextView.translatesAutoresizingMaskIntoConstraints = false
        
     /*   NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])*/
        
        NSLayoutConstraint.activate([
              titleTextView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
              titleTextView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
              titleTextView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
              titleTextView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
          ])
        
    }
  }

