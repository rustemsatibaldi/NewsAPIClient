//
//  QueryResultCell.swift
//  CountryAPIClient
//
//  Created by Main User on 18.10.2020.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit

class QueryResultCell: UITableViewCell {
    
    //MARK: - UI Controls
    lazy var articleImageView:UIImageView = {
        let articleImage = UIImageView()
        articleImage.clipsToBounds = true
        articleImage.contentMode = .scaleAspectFill
        
        articleImage.layer.cornerRadius = 7
        return articleImage
    }()
    
    lazy var titleLabel:UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .left
        titleLabel.font = .systemFont(ofSize: 17, weight: .bold)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 0
        titleLabel.text = "Bla"
        titleLabel.sizeToFit()
        return titleLabel
    }()
    
    lazy var sourceLabel:UILabel = {
        let sourceLabel = UILabel()
        sourceLabel.font = UIFont(name: "Avenir", size: 15)
        return sourceLabel
    }()
    
    lazy var datePublishedLabel:UILabel = {
        let datePublishedLabel = UILabel()
        datePublishedLabel.textAlignment = .right
        datePublishedLabel.font = UIFont(name: "Avenir", size: 14)
        return datePublishedLabel
    }()
    
    let containerView:UIView = {
        let containerView = UIView()
        containerView.backgroundColor = .systemGray
        containerView.clipsToBounds = true
        return containerView
    }()
    
    lazy var stackView:UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [sourceLabel, datePublishedLabel])
        stackView.axis = .horizontal
        stackView.spacing = 10
        return stackView
    }()
    
   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(containerView)
        self.contentView.addSubview(articleImageView)
        self.contentView.addSubview(titleLabel)
        //   self.contentView.addSubview(sourceLabel)
        //  self.contentView.addSubview(datePublishedLabel)
        self.contentView.addSubview(stackView)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK: - Setup constraints
    func setupConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        articleImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        //   sourceLabel.translatesAutoresizingMaskIntoConstraints = false
        //   datePublishedLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor),
            containerView.leftAnchor.constraint(equalTo:self.contentView.leftAnchor),
            containerView.rightAnchor.constraint(equalTo:self.contentView.rightAnchor),
            containerView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            articleImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            articleImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            articleImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            articleImageView.widthAnchor.constraint(equalToConstant: 70),
            articleImageView.heightAnchor.constraint(equalToConstant: 70)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: articleImageView.rightAnchor, constant: 10),
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            stackView.leftAnchor.constraint(equalTo: articleImageView.rightAnchor, constant: 10),
            stackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5)
        ])
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
