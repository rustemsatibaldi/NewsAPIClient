//
//  FavoritesCell.swift
//  CountryAPIClient
//
//  Created by Main User on 01.12.2020.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit

class FavoritesCell: UITableViewCell {
    
    var feedVC:NewsFeedViewController?
    
    
    let HSBColor = UIColor(hue: 220, saturation: 69, brightness: 60, alpha: 0.1)
    
    //MARK: - UI Controls
    lazy var articleImageView:UIImageView = {
        let articleImage = UIImageView()
        articleImage.clipsToBounds = true
        articleImage.contentMode = .scaleAspectFill
        articleImage.layer.cornerRadius = 7
        return articleImage
    }()
    
    lazy var titleLabel:UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .left
        titleLabel.font = .systemFont(ofSize: 17, weight: .bold)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 0
        titleLabel.sizeToFit()
        return titleLabel
    }()
    
    lazy var sourceLabel:UILabel = {
        let sourceLabel = UILabel()
        sourceLabel.font = UIFont(name: "Avenir", size: 15)
        return sourceLabel
    }()
    
    lazy var datePublishedLabel:UILabel = {
        let datePublishedLabel = UILabel()
        datePublishedLabel.textAlignment = .right
        datePublishedLabel.font = UIFont(name: "Avenir", size: 14)
        return datePublishedLabel
    }()
    
    
    let containerView:UIView = {
        let containerView = UIView()
        containerView.clipsToBounds = true
        
        return containerView
    }()
    
    lazy var stackView:UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [sourceLabel, datePublishedLabel])
        stackView.axis = .horizontal
        stackView.spacing = 10
        return stackView
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // contentView.backgroundColor = UIColor(named: "spaceCadet")
        
        self.contentView.addSubview(containerView)
        self.contentView.addSubview(articleImageView)
        self.contentView.addSubview(titleLabel)
        //   self.contentView.addSubview(sourceLabel)
        //  self.contentView.addSubview(datePublishedLabel)
        self.contentView.addSubview(stackView)
        //  self.layer.cornerRadius = 10
        //  self.layer.masksToBounds = true
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - Setup Constraints
    func setupConstraints() {
        
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        articleImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        //   sourceLabel.translatesAutoresizingMaskIntoConstraints = false
        //   datePublishedLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor),
            containerView.leftAnchor.constraint(equalTo:self.contentView.leftAnchor),
            containerView.rightAnchor.constraint(equalTo:self.contentView.rightAnchor),
            containerView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            articleImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            articleImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 15),
            articleImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            articleImageView.widthAnchor.constraint(equalToConstant: 70),
            articleImageView.heightAnchor.constraint(equalToConstant: 70)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: articleImageView.rightAnchor, constant: 10),
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            stackView.leftAnchor.constraint(equalTo: articleImageView.rightAnchor, constant: 10),
            stackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5)
        ])
        
        /*    NSLayoutConstraint.activate([
         sourceLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
         sourceLabel.leftAnchor.constraint(equalTo: articleImageView.rightAnchor, constant: 10)
         ])
         
         NSLayoutConstraint.activate([
         datePublishedLabel.topAnchor.constraint(equalTo: sourceLabel.bottomAnchor),
         datePublishedLabel.leftAnchor.constraint(equalTo: articleImageView.rightAnchor, constant: 10)
         ])*/
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //  articleImageView.frame = CGRect(x: 10, y: 10, width: 80, height: contentView.frame.size.height - 10)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        articleImageView.image = nil
    }
}

