//
//  Notification.swift
//  CountryAPIClient
//
//  Created by Main User on 04.08.2021.
//  Copyright © 2021 SwiftFlutter. All rights reserved.
//

import Foundation

struct Notification {
    var id: String
    var title: String
    var dateTime: DateComponents
}
