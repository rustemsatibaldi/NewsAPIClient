//
//  Country.swift
//  CountryAPIClient
//
//  Created by SwiftFlutter on 9/6/20.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import Foundation

struct ArticleData: Codable {
    let status: String?
    let totalResults: Int?
    let articles: [Article]?
    
    
      /*  init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            status = try container.decode(String?.self, forKey: .status)
              totalResults = try container.decode(Int?.self, forKey: .totalResults)
              articles = try container.decode([Article]?.self, forKey: .articles)
    }*/
}

// MARK: - Article
struct Article: Codable {
    let source: Source?
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: Date?
    let content: String?
  /*  enum CodingKeys: String, CodingKey {
        case source, author, title,
        case url, urlToImage, publishedAt, content
    }*/
}
// MARK: - Source
struct Source: Codable {
    let id: String?
    let name: String?
}

/*enum CodingKeys: String, CodingKey {
    case id = "id"
    case name = "name"
}*/



