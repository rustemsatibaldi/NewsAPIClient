//
//  Constants.swift
//  CountryAPIClient
//
//  Created by Main User on 03.11.2020.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import Foundation

class Constants {
    static let shared = Constants()
    let apiKey = "ab8da89cb5104c45b0be28d6144341f0"
    let urlString = "http://newsapi.org/v2/top-headlines?"
    let urlAdress = "http://newsapi.org/v2/top-headlines?country=us&apiKey=ab8da89cb5104c45b0be28d6144341f0"
    let countryCode = CountryCode.self
    //GET https://newsapi.org/v2/top-headlines?country=de&category=business&apiKey=API_KEY

    
    //mediastack
    let msUrlString = "https://api.mediastack.com/v1/news?"
    
}

enum URLEndpoints: String {
    case top = "top-headlines"
    case everyThing = "everything"
}

enum Category: String {
    case general = "general"
    case business = "business"
    case health = "health"
    case technology = "tecnology"
    case science = "science"
}

public enum CountryCode: String {
    case ru = "ru"
    case us = "us"
    case kr = "kr"
    case gb = "gb"
    case fr = "fr"
    case it = "it"
    case sa = "sa"
    case gr = "gr"
    case ua = "ua"
    case br = "br"
    case ch = "ch"
    case au = "au"
    case cn = "cn"
    case tr = "tr"
}

//news language
enum NewsLanguage: String {
    case ar = "ar"
    case de = "de"
    case en = "en"
    case es = "es"
    case fr = "fr"
    case he = "he"
    case it = "it"
    case nl = "nl"
    case no = "no"
    case pt = "pt"
    case ru = "ru"
    case se = "se"
    case ud = "ud"
    case zh = "zh"
    case none = ""
}


//enum for localization extension
enum Language: String {
    case none = ""
    case en = "English"
    case fr = "French"
    case it = "Italian"
    case ar = "Arabic"
    case de = "German"
    case es = "Spanish"
    case pt = "Portugese"
    case ru = "Russian"
}

