//
//  NotificationsManager.swift
//  CountryAPIClient
//
//  Created by Main User on 04.08.2021.
//  Copyright © 2021 SwiftFlutter. All rights reserved.
//

import Foundation
import UserNotifications


class NotificationsManager: NSObject {
    static let notificationsManager = NotificationsManager()
    var notifications = [Notification]()

    func listScheduledNotifications() {
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in
            for notification in notifications {
                print(notification)
            }
        }
    }
    
    private func requestAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
        
            if granted == true && error == nil {
                self.scheduleNotifications()
            }
    }
    }
    
    func schedule() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization()
            case .authorized:
                self.scheduleNotifications()
            default:
                break
            }
        }
    }
    
    private func scheduleNotifications() {
        for notification in notifications {
            let content = UNMutableNotificationContent()
            content.title = notification.title
            content.sound = .default
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: notification.dateTime, repeats: false)

                    let request = UNNotificationRequest(identifier: notification.id, content: content, trigger: trigger)

                    UNUserNotificationCenter.current().add(request) { error in

                        guard error == nil else { return }

                        print("Notification scheduled! --- ID = \(notification.id)")
                    }
                }
            }
}



