//
//  ArticleService.swift
//  CountryAPIClient
//
//  Created by SwiftFlutter on 9/6/20.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import Foundation

struct ArticleService {
    
    static let shared = ArticleService()
    
    let API_KEY = "ab8da89cb5104c45b0be28d6144341f0"
    let urlString = "https://newsapi.org/v2/top-headlines?"
    
    let session = URLSession.shared
    let countryCode = Constants.shared.countryCode
    
    func getArticleURL() -> String {
        return "\(urlString)country=\(countryCode.us)&apiKey=\(API_KEY)"
    }
    
    func getArticlesByCategory(category: Category) -> String {
        print("\(Constants.shared.urlString)country=us&category=\(String(describing: category))&apiKey=\(API_KEY)")
        return "\(Constants.shared.urlString)country=us&category=\(category)&apiKey=\(API_KEY)"
    }
    
    func searchArticle(query: String) -> String {
        return "https://newsapi.org/v2/everything?q=\(query)"
    }
    
    func getArticles(url: String?, completion: @escaping (ArticleData?, Error?)-> Void) {
        guard let url = URL(string: url!) else { return
        }
            session.dataTask(with: url) { data, response, error in
                if let error = error {
                    print(error.localizedDescription)
                    completion(nil, error)
                    return
                }
                guard let data = data else { return }
               // let jsonString = String(data: data, encoding: .utf8)
               // print(jsonString)
                  do {
                      let decoder = JSONDecoder()
                       decoder.dateDecodingStrategy = .iso8601
                    
                      let article = try decoder.decode(ArticleData.self, from: data)
                    completion(article, nil)
                
                  /*  article.articles?.forEach({ elements in
                        print(elements.author)
                    })*/
                    
                  } catch let error {
                      print(error)
                    completion(nil, error)
                  }
        }.resume()
    }
    
    func searchByKeyWord(completion: @escaping (ArticleData?, Error?) -> Void) {
        guard let url = URL(string: ArticleService.shared.searchArticle(query: "")) else { return }
        
        session.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error)
                completion(nil, error)
                return
            }
            guard let data = data else { return }
            print("minau degen : \(data)")
            do {
                let searchDecoder = JSONDecoder()
                let foundArticle = try searchDecoder.decode(ArticleData.self, from: data)
                print(foundArticle)
            } catch {
                print(error)
            }
            
            guard let response = response else { return }
            print("response result is: \(response)")
        }
    }
    
}
