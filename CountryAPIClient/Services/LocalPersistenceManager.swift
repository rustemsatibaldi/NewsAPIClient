//
//  LocalPersistenceManager.swift
//  CountryAPIClient
//
//  Created by Main User on 31.07.2021.
//  Copyright © 2021 SwiftFlutter. All rights reserved.
//

import Foundation
import RealmSwift

class LocalPersistenceManager {
    static let shared = LocalPersistenceManager()
    
    let realm = try! Realm()
    let articleData = LocalArticleData()
    var article = LocalArticle()
    
    var objectNotificationToken: NotificationToken?
       
    
  //  func getAll() {
     //   let articles = realm.objects(LocalArticle.self)
  //  }
    
    func saveArticleData(articleParam: LocalArticle) {
          article = articleParam
        
        try! realm.write {
        realm.add(article)
        }
    }
    func deleteArticleData(articleToDelete: LocalArticle) {
       // let articleToDelete = article[0]
        
        try! realm.write {
            realm.delete(articleToDelete)
        }
    }
}
