//
//  Extension+Language.swift
//  CountryAPIClient
//
//  Created by Main User on 30.07.2021.
//  Copyright © 2021 SwiftFlutter. All rights reserved.
//

import Foundation

/*enum Language: String {
    case none = ""
    case en = "English"
    case fr = "French"
    case it = "Italian"
    case ar = "Arabic"
    case de = "German"
    case es = "Spanish"
    case pt = "Portugese"
    case ru = "Russian"
}*/

extension Locale {
    static var enLocale: Locale {
return Locale(identifier: "en-EN")
    }
    static var currentLanguage: Language? {
        guard let code = preferredLanguages.first?.components(separatedBy: "-").last else {
           print("Could not detect language code")
            
            return nil
        }
        
        guard let rawValue = enLocale.localizedString(forLanguageCode: code) else {
            print("could not localize language code")
            
            return nil
        }
        
        guard let language = Language(rawValue: rawValue)  else {
            print("could not init language from raw value")
            return nil
        }
        print("language: \(code)-\(rawValue)")
        return language
    }
    
}

//Usage of this:
/*
 if let currentLanguage = Locale.currentLanguage {
     print(currentLanguage.rawValue)
     // Your code here.
 }
 */
