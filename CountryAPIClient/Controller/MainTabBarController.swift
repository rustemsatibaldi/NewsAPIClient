//
//  MainTabBarController.swift
//  CountryAPIClient
//
//  Created by SwiftFlutter on 9/14/20.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

   let network = ReachabilityManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor = UIColor(named: "greenCarib")
       network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }
        
        viewControllers = [
            generateNavController(rootViewController: NewsFeedViewController(), title: "Feed", imageName: UIImage(named: "rss")),
            generateNavController(rootViewController: SearchViewController(), title: "Search", imageName: UIImage(named: "Search More")),
            generateNavController(rootViewController: FavoritesViewController(), title: "Favorites", imageName: UIImage(named: "Pin")),
      //      generateNavController(rootViewController: SettingsViewController(), title: "Settings", imageName: UIImage(named: "Gear"))
        ]
    }
    
    
    func showOfflinePage() {
        DispatchQueue.main.async {
            let offlineVC = OfflineViewController()
         //   self.navigationController!.pushViewController( offlineVC, animated: true)
            offlineVC.modalPresentationStyle = .fullScreen

            self.present(offlineVC, animated: true, completion: nil)
        }
    }
    
    func generateNavController(rootViewController: UIViewController, title: String, imageName: UIImage?) -> UIViewController {
        let navigationVC = UINavigationController(rootViewController: rootViewController)
      
        navigationVC.tabBarItem.title = title
        navigationVC.tabBarItem.image = imageName
        navigationVC.navigationBar.prefersLargeTitles = true
        
      
      //  navigationVC.navigationBar.backgroundColor = .systemRed
        return navigationVC
    }
}
