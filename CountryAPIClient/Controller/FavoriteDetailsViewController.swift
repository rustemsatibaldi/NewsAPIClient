//
//  FavoriteDetailsViewController.swift
//  CountryAPIClient
//
//  Created by Main User on 31.07.2021.
//  Copyright © 2021 SwiftFlutter. All rights reserved.
//

import UIKit

class FavoriteDetailsViewController: UIViewController {

     //   var articles:[LocalArticle]
        var titleTxt = ""
        var contentTxt = ""
        var descriptionTxt = "bla"
        var urlFromJSON = ""
        //   let url = URL(string: urlFromJSON)!
        // let urlRequest = URLRequest(url: url)
    
        let articleDetailHeaderView = ArticleDetailHeaderView()
        
        //  public var cellDataCompletion:((String?) -> ())?
        private lazy var detailsTableView:UITableView = {
            let detailsTableView = UITableView()
            detailsTableView.isScrollEnabled = false
            detailsTableView.allowsSelection = false
            //  detailsTableView.contentInsetAdjustmentBehavior = .never
            return detailsTableView
        }()
        
        
     /*   init(articles:[LocalArticle]) {
            self.articles = articles
            super.init(nibName: nil, bundle: nil)
        }*/
        
     /*   required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }*/
        
        //MARK: - ViewController LifeCycle
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            navigationController?.hidesBarsOnSwipe = false
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
        //    let barButton = UIBarButtonItem(customView:articleDetailHeaderView.saveToFavoritesButton)
         //   self.navigationItem.rightBarButtonItem = barButton
            setupUI()
            setupConstraints()
        }
        
        //MARK: - Set up UI
        func setupUI() {
            view.backgroundColor = .white
            detailsTableView.register(ArticleDetailTableCell.self, forCellReuseIdentifier: "detailCell")
            detailsTableView.register(ArticleTitleTableCell.self, forCellReuseIdentifier: "titleCell")
            detailsTableView.register(ReadFullButtonCell.self, forCellReuseIdentifier: "readFullCell")
            
            /*       webView.frame = view.bounds
             webView.autoresizingMask = [.flexibleWidth,.flexibleHeight]*/
            detailsTableView.delegate = self
            detailsTableView.dataSource = self
            detailsTableView.separatorStyle = .none
            //  webView.navigationDelegate = self
            //  navigationItem.largeTitleDisplayMode = .never
            view.addSubview(articleDetailHeaderView)
            view.addSubview(detailsTableView)
            //view.addSubview(goToWebsiteButton)
        }
        
        //MARK: - setup constraints
        func setupConstraints() {
            
            articleDetailHeaderView.translatesAutoresizingMaskIntoConstraints = false
           detailsTableView.translatesAutoresizingMaskIntoConstraints = false
          //  goToWebsiteButton.translatesAutoresizingMaskIntoConstraints = false
            
            
            NSLayoutConstraint.activate([
                articleDetailHeaderView.topAnchor.constraint(equalTo: view.topAnchor),
                articleDetailHeaderView.leftAnchor.constraint(equalTo: detailsTableView.leftAnchor),
                articleDetailHeaderView.rightAnchor.constraint(equalTo: detailsTableView.rightAnchor),
                articleDetailHeaderView.heightAnchor.constraint(equalToConstant: 320)
            ])

            NSLayoutConstraint.activate([
                  detailsTableView.topAnchor.constraint(equalTo: articleDetailHeaderView.bottomAnchor),
                  detailsTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
                  detailsTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
               //   detailsTableView.heightAnchor.constraint(equalToConstant: 130),
                  detailsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
              ])
            
        /*    NSLayoutConstraint.activate([
                goToWebsiteButton.topAnchor.constraint(equalTo: detailsTableView.bottomAnchor, constant: 30),
                goToWebsiteButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 50),
                goToWebsiteButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -50),
                //    goToWebsiteButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                goToWebsiteButton.widthAnchor.constraint(equalToConstant: 60),
                goToWebsiteButton.heightAnchor.constraint(equalToConstant: 50)
            ])*/
        }
}

    //MARK: - EXTENSIONS
    extension FavoriteDetailsViewController: UITableViewDataSource, UITableViewDelegate {
        
        @objc func goToWebSite() {
            if let url = URL(string: urlFromJSON) {
                UIApplication.shared.open(url)
            }
            /* guard let url = URL(string: urlFromJSON) else { return }
             webView.load(URLRequest(url: url))
             webView.allowsBackForwardNavigationGestures = true*/
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
               return 1
           }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath) as! ArticleTitleTableCell
                cell.titleTextView.text = titleTxt
                self.detailsTableView.rowHeight = 130
                return cell
            
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! ArticleDetailTableCell
               cell.contentTextView.text = contentTxt
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "readFullCell", for: indexPath) as! ReadFullButtonCell
              // cell.contentTextView.text = contentTxt
                cell.goToWebsiteButton.addTarget(self, action: #selector(goToWebSite), for: .touchUpInside)
                return cell
                
            default:
                fatalError("Failed to instantiate the table view cell for detail view controller")
            }
         //   cell.titleLabel.text = titleTxt
        }
        
    }



