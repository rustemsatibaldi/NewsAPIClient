//
//  SettingsViewController.swift
//  CountryAPIClient
//
//  Created by Main User on 01.08.2021.
//  Copyright © 2021 SwiftFlutter. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    lazy var settingsTableView:UITableView = {
        let tableView = UITableView()
        
        return tableView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
setupUI()
        setupConstraints()
        // Do any additional setup after loading the view.
    }
    //MARK: - SETUP UI
    func setupUI() {
        
       
        navigationItem.title = "SETTINGS"
    
         navigationController?.navigationBar.backgroundColor = .gray
      
    }
    func setupConstraints() {
        settingsTableView.frame = view.bounds
        view.addSubview(settingsTableView)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
