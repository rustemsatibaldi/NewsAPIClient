//
//  ViewController.swift
//  CountryAPIClient
//
//  Created by SwiftFlutter on 9/6/20.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit
import SDWebImage
import HMSegmentedControl
//import Reachability

class NewsFeedViewController:UIViewController {
    
    let segmentedControl = HMSegmentedControl(sectionTitles:
                                                [
                                                    "General",
                                                    "Business",
                                                    "Health",
                                                    "Science",
                                                    "Technology"
                                                ])
    let manager = NotificationsManager()
    let calendar = Calendar.current
    let date = Date()
    lazy var year = calendar.component(.year, from: date)
   lazy var month = calendar.component(.month, from: date)
    lazy var dayOfWeek = calendar.component(.weekday, from: date)
   lazy var day = calendar.component(.day, from: date)
    var news = [Article]()
    let network: ReachabilityManager = ReachabilityManager.sharedInstance
    let localRealmStorage = LocalPersistenceManager()
    var categoryUrlString = ArticleService.shared.getArticlesByCategory(category: Category.general)
 
    private lazy var newsTableView: UITableView = {
        let newsTableView = UITableView()
        // newsTableView.estimatedRowHeight = 100
        //    newsTableView.rowHeight = UITableView.automaticDimension
        return newsTableView
    }()
    
    var loadingSpinner:UIActivityIndicatorView = {
        let loadingSpinner = UIActivityIndicatorView()
        loadingSpinner.style = .gray
        loadingSpinner.hidesWhenStopped = true
        return loadingSpinner
    }()
    
    var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        return refreshControl
    }()
    
    //MARK: - View Controller LIFECYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupConstraints()
        getArticles(urlWithCategory: categoryUrlString)
        manager.schedule()
    }
   
    func saveToLocal(index: Int) {
        let localArticle = LocalArticle()
        localArticle.title = news[index].title
        localArticle.articleDescription = news[index].description
        localArticle.author = news[index].author
        localArticle.content = news[index].content
        localArticle.publishedAt = news[index].publishedAt
        localArticle.source?.name = news[index].source?.name
        localArticle.url = news[index].url
        localArticle.urlToImage =  news[index].urlToImage
        localRealmStorage.saveArticleData(articleParam: localArticle)
        
        print("localArticle \(localArticle)")
    }
    
    //MARK: - SETUP UI
    func setupUI() {
        view.backgroundColor = .white
        segmentedControl.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        segmentedControl.selectionIndicatorColor = UIColor(named: "greenCarib") ?? .black
        segmentedControl.selectionIndicatorLocation = .bottom
        segmentedControl.addTarget(self, action: #selector(controlValueChanged), for: .valueChanged)
        
        
        newsTableView.register(ArticleCell.self, forCellReuseIdentifier: "Cell")
        newsTableView.dataSource = self
        newsTableView.delegate = self
        // topCategoriesCollectionView.dataSource = self
        //  topCategoriesCollectionView.delegate = self
        // newsTableView.bounces = false
        newsTableView.separatorColor = .blue
        newsTableView.separatorStyle = .none
        //  navigationController?.isNavigationBarHidden = true
        newsTableView.estimatedRowHeight = 100
        newsTableView.rowHeight = UITableView.automaticDimension
        //  navigationController?.navigationBar.backgroundColor = .systemRed
        loadingSpinner.startAnimating()
        //  view.addSubview(topCategoriesCollectionView)
        view.addSubview(segmentedControl)
        view.addSubview(newsTableView)
        view.addSubview(loadingSpinner)
    }
    
    //MARK: - SETUP Constraints
    func setupConstraints() {
        //  topCategoriesCollectionView.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        newsTableView.translatesAutoresizingMaskIntoConstraints = false
        loadingSpinner.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([ loadingSpinner.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 150.0),
                                      loadingSpinner.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
        
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            segmentedControl.leftAnchor.constraint(equalTo: view.leftAnchor),
            segmentedControl.rightAnchor.constraint(equalTo: view.rightAnchor),
            segmentedControl.heightAnchor.constraint(equalToConstant: 50)
        ])

        NSLayoutConstraint.activate([
            newsTableView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor),
            //  newsTableView.topAnchor.constraint(equalTo: view.topAnchor),
            newsTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            newsTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            newsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
  
    @objc func controlValueChanged() {
        switch segmentedControl.selectedSegmentIndex {
      /*  case 0:
            print("General")
         categoryUrlString =   ArticleService.shared.getArticlesByCategory(category: Category.general)
            getArticles(urlWithCategory: categoryUrlString)*/

        case 1:
            print("Business")
            categoryUrlString =  ArticleService.shared.getArticlesByCategory(category: Category.business)
            getArticles(urlWithCategory: categoryUrlString)
        case 2:
            print("health")
            categoryUrlString =  ArticleService.shared.getArticlesByCategory(category: Category.health)
            getArticles(urlWithCategory: categoryUrlString)
        case 3:
            print("science")
            categoryUrlString =  ArticleService.shared.getArticlesByCategory(category: Category.science)
            getArticles(urlWithCategory: categoryUrlString)
        case 4:
            print("technology")
            categoryUrlString =  ArticleService.shared.getArticlesByCategory(category: Category.technology)
            getArticles(urlWithCategory: categoryUrlString)
        default:
            categoryUrlString =   ArticleService.shared.getArticlesByCategory(category: Category.general)
              getArticles(urlWithCategory: categoryUrlString)
        }
    }
    
    func getArticles(urlWithCategory: String?) {
        ArticleService.shared.getArticles(url: urlWithCategory, completion: { [self]
            newsArticles, error in
        
            self.news = newsArticles.map({ $0.articles }) as? [Article] ?? []
       //   print(error?.localizedDescription)
            //   self.news = newsArticles?.articles as [Article]
            self.manager.notifications = [ Notification(id: "reminder-1", title: self.news[0].title ?? "news", dateTime: DateComponents(calendar: Calendar.current, year: year, month: month, day: day, hour: 11, minute: 31, weekday: dayOfWeek)),
            ]
            
            DispatchQueue.main.async {
                self.loadingSpinner.stopAnimating()
                self.newsTableView.reloadData()
            }
        })
    }
}

//MARK: - Extensions for VC
extension NewsFeedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return news.count
   
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ArticleCell
        cell.layer.cornerRadius = 12
        cell.selectionStyle = .blue
      
       let newsArticle = news[indexPath.row]
        cell.titleLabel.text = newsArticle.title
        cell.sourceLabel.text = newsArticle.source?.name
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, H:mm"
        cell.datePublishedLabel.text =         dateFormatter.string(from: newsArticle.publishedAt!)
        
        if let url = URL( string:news[indexPath.row].urlToImage ?? "")
        {
            DispatchQueue.global().async {
                DispatchQueue.main.async {
                    // cell.articleImageView.image = UIImage( data:data)
                    cell.articleImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"), completed: nil)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //  let detailVC = ArticleDetailController(articles: news)
        let detailVC = ArticleDetailsViewController(articles: news)
        let url1 = URL(string: news[indexPath.row].urlToImage ?? "")
        // if let url = URL(string:news[indexPath.row].urlToImage ?? "") {
        //  detailVC.articleDetailHeader.detailImageView.sd_setImage(with: url1, completed: nil)
        detailVC.articleDetailHeaderView.detailImageView.sd_setImage(with: url1, completed: nil)
        //   }
        detailVC.titleTxt = news[indexPath.row].title ?? "blank"
        detailVC.urlFromJSON = news[indexPath.row].url ?? "link"
        detailVC.contentTxt = news[indexPath.row].description ?? "default"
        detailVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailVC, animated: false)
    }
}

//MARK: - Extension for TableView Delegate
extension NewsFeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        // var fav = news[indexPath.row]
        let action = UIContextualAction(style: .normal, title: "Favorite") {
            (action, view, completionHandler) in
            // self.handleMarkAsFavorite()
            self.saveToLocal(index: indexPath.row)
            print("indexpath \(indexPath.row)")
            completionHandler(true)
        }
        action.backgroundColor = .systemGreen
        action.image = #imageLiteral(resourceName: "Pin")
        return UISwipeActionsConfiguration(actions: [action])
    }
}

