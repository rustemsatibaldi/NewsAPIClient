//  SearchViewController.swift
//  CountryAPIClient
//  Created by SwiftFlutter on 9/14/20.
//  Copyright © 2020 SwiftFlutter. All rights reserved.

import UIKit

class SearchViewController: UIViewController {
    
    private let reuseIdentifier = "cell"
    var searchResults = [Article]()
    //    var news = [Article]()
    var searchController: UISearchController!
    private var timer: Timer?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupSearchBar()
        setupConstraints()
      
        // self.resultsTableView.reloadData()
    }
    
    private lazy var resultsTableView:UITableView = {
        let resultsTableView = UITableView()
        return resultsTableView
    }()
    
    //MARK: - Setup UI Elements
    func setupUI() {
        view.backgroundColor = .systemGray //UIColor(named: "celtricBlue")
        navigationItem.title = "SEARCH"
        resultsTableView.register(ArticleCell.self, forCellReuseIdentifier: reuseIdentifier)
        resultsTableView.delegate = self
        resultsTableView.dataSource = self
        resultsTableView.separatorStyle = .none
        view.addSubview(resultsTableView)
    }
        
    func setupConstraints() {
        resultsTableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            resultsTableView.topAnchor.constraint(equalTo: view.topAnchor),
            resultsTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            resultsTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            resultsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func setupSearchBar() {
        searchController = UISearchController(searchResultsController: nil)
        self.navigationItem.searchController = searchController
        //   navigationItem.title = "SEARCH"
        // navigationController?.navigationBar.prefersLargeTitles = true
        searchController.searchResultsUpdater = self
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        let searcher = searchController.searchBar
        searcher.sizeToFit()
    }
}

//MARK: - Extension SearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchUrl =  "https://newsapi.org/v2/everything?q=\(searchText)&apiKey=ab8da89cb5104c45b0be28d6144341f0"
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: {(_) in
            ArticleService.shared.getArticles(url: searchUrl, completion: {
                (newsArticles, error) in
                self.searchResults = newsArticles.map({ $0.articles }) as? [Article] ?? []
                //print("Ruseke: \(self.searchResults)")
                DispatchQueue.main.async {
                    self.resultsTableView.reloadData()
                }
            })
        })
        
    }
    /*    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
     
     }*/
}

// MARK: - Extension Tableview Delegate
extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("the count is: \(searchResults.count)")
        if searchResults.count == 0 {
            self.resultsTableView.setEmptyMessage("Nothing to show")
        } else {
            self.resultsTableView.restore()
        }
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ArticleCell//QueryResultCell
        let article = searchResults[indexPath.row]
        cell.layer.cornerRadius = 12
        
        cell.titleLabel.text = article.title
        cell.sourceLabel.text = article.source?.name
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, H:mm"
        cell.datePublishedLabel.text = dateFormatter.string(from: article.publishedAt!)
        if let url = URL( string: searchResults[indexPath.row].urlToImage ?? "")
        {
            
            // cell.articleImageView.image = UIImage( data:data)
            cell.articleImageView.sd_setImage(with: url, completed: nil)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = SearchDetailsController(articles: searchResults)
        let url1 = URL(string: searchResults[indexPath.row].urlToImage ?? "")!
        detailVC.articleDetailHeaderView.detailImageView.sd_setImage(with: url1, completed: nil)
        //   }
        detailVC.titleTxt = searchResults[indexPath.row].title ?? "blank"
        detailVC.urlFromJSON = searchResults[indexPath.row].url ?? "link"
        detailVC.contentTxt = searchResults[indexPath.row].description ?? "default"
        
        detailVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailVC, animated: false)
    }
}

extension SearchViewController: UITableViewDelegate {
    
}


extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterContent(for: searchText)
        }
    }
    
    func filterContent(for searchText: String) {
        /*    searchResults = news.filter({ (news) -> Bool in
         if let title = news.title {
         let isMatch = title.localizedCaseInsensitiveContains(searchText)
         return isMatch
         }
         return false
         })*/
        //   self.resultsTableView.reloadData()
    }
}

