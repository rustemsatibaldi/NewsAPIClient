//
//  FavoritesViewController.swift
//  CountryAPIClient
//
//  Created by SwiftFlutter on 9/16/20.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit
import RealmSwift

let reuseIdentifier = "cell"

class FavoritesViewController: UIViewController {

    var favorites = [LocalArticle]()
    var favoriteArticles = LocalPersistenceManager.shared.realm.objects(LocalArticle.self).sorted(byKeyPath: "_id")
    var notificationToken = LocalPersistenceManager.shared.objectNotificationToken
    
    lazy var favoritesTableView:UITableView = {
        let favoritesTableView = UITableView()
        
        return favoritesTableView
    }()
   
    //MARK: - UIViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        setupUI()
        
        
        notificationToken  = favoriteArticles.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.favoritesTableView else { return }
            
            switch changes {
            case .initial:
                self?.favoritesTableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({IndexPath(row: $0, section: 0)}), with: .automatic)
                tableView.deleteRows(at: deletions.map({IndexPath(row: $0, section: 0)}), with: .automatic)
                tableView.reloadRows(at: modifications.map({IndexPath(row: $0, section: 0)}), with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        self.favoritesTableView.reloadData()
    }
    
   //MARK: - Setup UI
    func setupUI() {
        navigationItem.title = "FAVORITES"
        favoritesTableView.register(FavoritesCell.self, forCellReuseIdentifier: reuseIdentifier)
        favoritesTableView.dataSource = self
        favoritesTableView.delegate = self
        favoritesTableView.separatorStyle = .none
        favoritesTableView.separatorColor = .white
        favoritesTableView.estimatedRowHeight = 100
        favoritesTableView.rowHeight = UITableView.automaticDimension
    }
    
    //MARK: - Setup Constraints
    func setupConstraints() {
        favoritesTableView.frame = view.bounds
        view.addSubview(favoritesTableView)
    }

    deinit {
    notificationToken?.invalidate()
    }
}

//MARK: - Extensions
extension FavoritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
if favoriteArticles.count == 0 {
            self.favoritesTableView.setEmptyMessage("No articles was added")
        } else {
            self.favoritesTableView.restore()
        }
        return favoriteArticles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FavoritesCell
        
        cell.layer.cornerRadius = 12
        cell.selectionStyle = .blue
       let favorite = favoriteArticles[indexPath.row]
        cell.titleLabel.text = favorite.title
        cell.sourceLabel.text = favorite.source?.name ?? "favorite"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, H:mm"
        cell.datePublishedLabel.text =  dateFormatter.string(from: favorite.publishedAt!)
        
        if let url = URL( string:favoriteArticles[indexPath.row].urlToImage ?? "")
        {
            DispatchQueue.global().async {
                DispatchQueue.main.async {
                    // cell.articleImageView.image = UIImage( data:data)
                    cell.articleImageView.sd_setImage(with: url, completed: nil)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = FavoriteDetailsViewController()
        let url1 = URL(string: favoriteArticles[indexPath.row].urlToImage ?? "")
        detailVC.articleDetailHeaderView.detailImageView.sd_setImage(with: url1, completed: nil)
        //   }
        detailVC.titleTxt = favoriteArticles[indexPath.row].title ?? "blank"
        detailVC.urlFromJSON = favoriteArticles[indexPath.row].url ?? "link"
        detailVC.contentTxt = favoriteArticles[indexPath.row].articleDescription ?? "ll"
        
        detailVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailVC, animated: false)
    }
}
extension FavoritesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Delete") {
            (action, view, completionHandler) in
        //write here
            let objectData = self.favoriteArticles[indexPath.row]
            LocalPersistenceManager.shared.deleteArticleData(articleToDelete: objectData)
            
            completionHandler(true)
        }
        action.backgroundColor = .systemRed
        action.image = #imageLiteral(resourceName: "mdi_delete")
        return UISwipeActionsConfiguration(actions: [action])
    }
}
