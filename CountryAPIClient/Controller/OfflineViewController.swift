//
//  OfflineViewController.swift
//  CountryAPIClient
//
//  Created by Main User on 08.10.2020.
//  Copyright © 2020 SwiftFlutter. All rights reserved.
//

import UIKit

class OfflineViewController: UIViewController {
    
    let network = ReachabilityManager.sharedInstance
    
    var imageView:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "no-connection")
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        return imageView
    }()
    
    var label:UILabel = {
       let label = UILabel()
        label.text = "Please, check connection"
      //  label.tintColor = .white
        label.font = UIFont(name: "Avenir", size: 21)
        label.textColor = .white
      //  label.baselineAdjustment = .alignCenters
        label.textAlignment = .center
        return label
    }()
    
    private lazy var updateButton:UIButton = {
       let updateButton = UIButton()
        updateButton.tintColor = .white
        updateButton.layer.cornerRadius = 30
        updateButton.layer.borderWidth = 2
        updateButton.setTitle("Retry", for: .normal)
        updateButton.layer.borderColor = UIColor.white.cgColor
       updateButton.addTarget(self, action: #selector(retryToConnect), for: .touchUpInside)
        return updateButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupConstraints()
        
        
      /* network.reachability.whenReachable = { _ in
                    self.showMainController()
                 }*/
    }
    
    //MARK: - Setup UI
    func setupUI() {
        view.backgroundColor = .gray
        view.addSubview(imageView)
        view.addSubview(label)
        view.addSubview(updateButton)
    
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    //MARK: - Setup constraints
    func setupConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        updateButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 90),
            imageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 50),
            imageView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -50)
        ])
       NSLayoutConstraint.activate([
        label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 15),
        label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40),
        label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40)
        ])
        
        NSLayoutConstraint.activate([
            updateButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10),
            updateButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40),
            updateButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40),
            updateButton.widthAnchor.constraint(equalToConstant: 60),
            updateButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func showMainController() {
        DispatchQueue.main.async {
            let mainVC = NewsFeedViewController()
            mainVC.modalPresentationStyle = .fullScreen
            self.present(mainVC, animated: true, completion: nil)      }

    }
   
    //MARK: - Selectors
  @objc func retryToConnect() {
   network.reachability.whenReachable = { _ in
                  
    self.showMainController()
              }
    network.reachability.whenUnreachable = { _ in
        print("Unable to connect")
    }
}
}
